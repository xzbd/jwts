package com.xzbd.log1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("log1")
public class Log1Controller {

    @RequestMapping("log")
    public String logTest() {

        String msg = "log from log1 :  ------->" + LocalDateTime.now();
        log.info(msg);
        return msg;
    }
}
