package com.xzbd.log2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("log2")
public class Log2Controller {

    @RequestMapping("log")
    public String logTest() {

        String msg = "log from log2 :  ------->" + LocalDateTime.now();
        log.info(msg);
        return msg;
    }
}
