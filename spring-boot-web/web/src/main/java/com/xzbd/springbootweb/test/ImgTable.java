package com.xzbd.springbootweb.test;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

public class ImgTable {
    public static void main(String[] args) throws Exception{

        
        String imgType = "png";

        String zipFileName = "img-zip-test.zip";

        // 输出流
        OutputStream outputStream = new FileOutputStream(zipFileName);
        // 如果是下载(记得设置下载头信息)
        // outputStream = response.getOutputStream();
        // zip 输出流
        ZipOutputStream zos = new ZipOutputStream(outputStream);
        // 压缩级别
        zos.setLevel(Deflater.BEST_COMPRESSION);

        for (int i = 0; i < 10; i++) {
            String imgName = i + "-test-img." + imgType;
        // 添加 test.txt 文件,并写入 10 行内容
        zos.putNextEntry(new ZipEntry(imgName));
            BufferedImage drawImg = drawImg("你好，\r\n IMG" + i);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(drawImg, imgType, os);
            byte[] byteArray = os.toByteArray();
            System.out.println("imgae-"+ i +"byteArray length: " + byteArray.length);
            zos.write(byteArray);
            zos.flush();
            zos.closeEntry();
        }
        zos.flush();
        zos.close();

    }

    public static BufferedImage drawImg(String content){
        int width = 100;
        int height = 80;

        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        
        // 所有可视范围设置为白色
        Graphics graphics = bi.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, width, height);

        // tbody
        graphics.setColor(Color.BLUE);
        graphics.drawLine(10, 10,75,10);
        graphics.drawLine(10, 10,10,20);
        graphics.drawLine(10, 20,75,20);
        graphics.drawLine(75, 10,75,20);
        graphics.setColor(Color.BLACK);
        graphics.drawString(content, 10, 20);


        return bi;
    }

}