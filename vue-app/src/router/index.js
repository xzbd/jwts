import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);


const routes = [
    {
        path: "/",
        redirect: "/home"
    },
    {
        path: '/home',
        component: () => import('@/pages/Home'),
        children:[
            {
                path: '/home/help',
                component: () => import('@/components/HelloWorld')
            }
        ]
    },
    {
        path: '/java',
        component: () => import('@/pages/java'),
        children: [
            {
                path: "/java/download/stream",
                component: () => import("@/pages/java/DownloaadStream")
            }
        ]
    },
    {
        path: '/web',
        component: () => import('@/pages/web'),
        children: [
        ]
    }
]

// 
const router =  new VueRouter({
    mode: 'history',
    routes
});

// 
export default router;