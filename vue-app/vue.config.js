module.exports = {
    // 开发服务器接口代理
    devServer: {
        port: 3000,
        proxy: {
        "/": {
            changeOrigin: true,
            target: "http://localhost:8081"
        }
        }
    },
    publicPath: process.env.NODE_ENV === 'production'
    ? '/'
    : '/'
}